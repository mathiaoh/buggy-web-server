#include "server_http.hpp"

typedef SimpleWeb::Server<SimpleWeb::HTTP> HttpServer;

int main() {
  // Construct HTTP-server at port 8080 using 1 thread, and without request timeout
  HttpServer server;
  server.config.port = 8080;
  server.config.timeout_request = 0;

  // Response to all GET requests
  server.resource["^.*$"]["GET"] = [](std::shared_ptr<HttpServer::Response> response, std::shared_ptr<HttpServer::Request> request) {
    try {
      if(std::stof(request->http_version) >= 1.1) {
        response->write("<h1>The web server is working!</h1>");
      }
    }
    catch(std::invalid_argument &e) {
      response->write(SimpleWeb::StatusCode::server_error_http_version_not_supported, "<h1>HTTP-version not valid (string)</h1>");
    }
    catch(std::out_of_range &e) {
      response->write(SimpleWeb::StatusCode::server_error_http_version_not_supported, "<h1>HTTP-version not valid (out of range)</h1>");
    }
  };

  server.start();
}
